# Lapres Praktikum Modul 4 Kelompok D07
Anggota Kelompok ''D07'' 
| Nama                      | NRP        |
|---------------------------|------------|
| Hanun Shaka Puspa         | 5025211051 |
| Mavaldi Rizqy Hazdi       | 5025211086 |
| Daffa Saskara             | 5025201249 |

## 1. Storage
### Penjelasan Solusi
Mengacu pada soal yang diberikan, akan dibuat file program bahasa C, Dockerfile, docker-compose.yml, dan file .txt.

#### Program ```storage.c```
Program C ini mengambil data statistik pemain dari Database Statistik Pemain FIFA dan menyaring hasilnya berdasarkan kriteria usia kurang dari 25 dan potensi lebih dari 85. Program ini mengunduh dataset, membaca data dari file CSV, dan menampilkan informasi untuk pemain yang memenuhi kondisi tertentu. Program ini menggunakan command ```kaggle```. Kaggle-cli dapat diinstall dengan mengikuti petunjuk yang disediakan di Kaggle CLI. Program juga memerlukan utilitas unzip terpasang pada sistem.

Beberapa command penting yang dipanggil dalam program ini meliputi:
1. Mengunduh dataset dari kaggle
```
kaggle datasets download -d bryanb/fifa-player-stats-database
```

2. Unzip dataset yang telah diunduh
```
unzip -o fifa-player-stats-database.zip
```

Output program akan ditampilkan di terminal dan memiliki format sebagai berikut
```
Nama: Nama Pemain
Usia: 24
Foto: player_photo.jpg
Kebangsaan: Negara
Rating Keseluruhan: 87
Rating Potensi: 90
Klub: Nama Klub
```

#### Dockerfile
Dockerfile ini menyediakan langkah-langkah yang diperlukan untuk mengemas program "storage.c" ke dalam sebuah kontainer dengan lingkungan Ubuntu. Dockerfile ini menginstal dependensi yang dibutuhkan, mengonfigurasi Kaggle CLI, mengompilasi program "storage.c", dan menjadikannya sebagai perintah default.

Untuk membuat docker image dari Dockerfile ini, dipanggil command berikut
```
docker build -t storage-app .
```
Kemudian image atau kontainer docker tersebut dapat dirun dengan 
```
docker run -it storage-app
```

#### docker-compose.yml
File docker-compose.yml ini berisi konfigurasi untuk menjalankan beberapa instance dari aplikasi Storage menggunakan Docker Compose. Setiap instance aplikasi akan dijalankan pada port yang berbeda.
Aplikasi Storage akan dijalankan pada port-port berikut:

Storage App 1: http://localhost:8001
Storage App 2: http://localhost:8002
Storage App 3: http://localhost:8003
Storage App 4: http://localhost:8004
Storage App 5: http://localhost:8005

Untuk menjalankan docker-compose tersebut, dipanggil command berikut.
```
docker-compose up -d
```

#### ```hubdocker.txt```
Untuk mengunggah docker image ke hubdocker, dipanggil beberapa perintah berikut.
```
docker login
```
Kemudian ikuti langkah-langkah di konsol untuk login ke akun hubdocker yang telah dibuat.
```
docker tag storage-app borednuna/storage-app
docker push borednuna/storage-app
```
Url ke repository docker image yang telah diunggah ke dockerhub ada di dalam file ```.txt``` ini.

### Screenshot Solusi Soal 1
<img src="https://i.ibb.co/ZzJTTtX/Screenshot-from-2023-06-03-19-08-49-1.png" width=700>
<img src="https://i.ibb.co/5Rkwm59/Screenshot-from-2023-06-03-19-25-00.png" width=700>
<img src="https://i.ibb.co/y5SyJZ5/Screenshot-from-2023-06-03-20-37-49.png" width=700>
### Kendala Pengerjaan Soal 1
Tidak banyak kendala pada pengerjaan soal ini.

## 2. Germa
### Penjelasan Solusi :
### **2a**
Pada bagian ini, kita akan membuat folder baru bernama productMagang di dalam folder /src_data/germa/products/restricted_list/. Selanjutnya, kita akan membuat folder projectMagang di dalam folder /src_data/germa/projects/restricted_list/. Namun, percobaan tersebut akan gagal.

```c
int isRestricted(const char *path)
{
  return strstr(path, "restricted") != NULL;
}

int isBypass(const char *path)
{
  return strstr(path, "bypass") != NULL;
}

static int xmp_mkdir(const char *path, mode_t mode)
{
  printf("doing mkdir\n");
  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  if (!isRestricted(fpath) || isBypass(fpath))
  {
    res = mkdir(fpath, mode);

    char desc[2000];
    snprintf(desc, sizeof(desc), "Create directory %s", fpath);
    writeLog("SUCCESS", "MKDIR", desc);
  }
  else
  {
    res = -EPERM;

    char desc[2000];
    snprintf(desc, sizeof(desc), "Create directory %s", fpath);
    writeLog("FAILED", "MKDIR", desc);
  }

  return res;
}
```

+ Fungsi isRestricted berfungsi untuk memeriksa apakah terdapat kata "restricted" dalam path yang diberikan. Jika kata tersebut ditemukan, fungsi ini akan mengembalikan nilai 1, sedangkan jika tidak ditemukan, fungsi ini akan mengembalikan nilai 0.
+ Fungsi isBypass berperan untuk memeriksa apakah terdapat kata "bypass" dalam path yang diberikan. Jika kata tersebut terdapat, fungsi ini akan mengembalikan nilai 1, tetapi jika tidak ditemukan, fungsi ini akan mengembalikan nilai 0.
+ Jika path yang diberikan tidak mengandung kata "restricted" dan juga mengandung kata "bypass", maka fungsi ini akan membuat folder sesuai dengan path yang telah ditentukan.

### **2b**
Ubahlah nama folder dari **restricted_list** pada folder **/src_data/germa/projects/restricted_list/** menjadi **/src_data/germa/projects/bypass_list/**. Kemudian, buat folder **projectMagang** di dalamnya.

```c
static int xmp_rename(const char *from, const char *to)
{
  printf("doing rename\n");

  char oldPath[1000], newPath[1000];
  snprintf(oldPath, sizeof(oldPath), "%s%s", dirpath, from);
  snprintf(newPath, sizeof(newPath), "%s%s", dirpath, to);

  int res = 0;
  if (!isRestricted(oldPath) || !isRestricted(newPath))
  {
    res = rename(oldPath, newPath);

    char desc[2500];
    snprintf(desc, sizeof(desc), "Rename from %s to %s", oldPath, newPath);
    writeLog("SUCCESS", "RENAME", desc);
  }
  else
  {
    res = -errno;

    char desc[2500];
    snprintf(desc, sizeof(desc), "Rename from %s to %s", oldPath, newPath);
    writeLog("FAILED", "RENAME", desc);
  }

  return res;
}
```
+ Jika path yang diberikan tidak mengandung kata "restricted" atau mengandung kata "bypass", maka fungsi ini akan mengubah nama folder menjadi nama yang telah ditentukan.
+ Proses pengubahan nama folder dilakukan dengan menggunakan perintah "mv oldPath newPath".

### **2c**
Karena folder projects telah dapat diakses, langkah selanjutnya adalah mengubah nama folder filePenting menjadi restrictedFilePenting agar keamanannya terjaga. Kita akan menguji keamanannya dengan mengubah nama file yang ada di dalamnya.
+ Sama seperti 2b, kita akan menggunakan ``mv oldPath newPath`` untuk merename folder tersebut terlebih dahulu.

### **2d**
Pada bagian ini, kita akan menghapus semua file yang berada di dalam folder restrictedFileLama. Jangan lupa untuk menambahkan kata sihir pada nama folder tersebut agar folder tersebut dapat dihapus.
```c
static int xmp_unlink(const char *path)
{
  printf("doing unlink\n");

  char fpath[1000];
  if (strcmp(path, "/") == 0)
  {
    path = dirpath;
    sprintf(fpath, "%s", path);
  }
  else
    sprintf(fpath, "%s%s", dirpath, path);

  int res = 0;

  if (!isRestricted(fpath) || isBypass(fpath))
  {
    res = unlink(fpath);

    char desc[2000];
    snprintf(desc, sizeof(desc), "Remove file %s", fpath);
    writeLog("SUCCESS", "UNLINK", desc);
  }
  else
  {
    res = -EPERM;

    char desc[2000];
    snprintf(desc, sizeof(desc), "Remove file %s", fpath);
    writeLog("FAILED", "UNLINK", desc);
  }

  return res;
}
```
+ Untuk menghapus semua ``fileLama``, kami harus mengubah folder ``restrictedFileLama`` dengan ``bypassFileLama`` terlebih dahulu.
+ Setelah itu, kita dapat menghapus semua ``fileLama`` dengan menggunakan ``rm -r bypassFileLama``.

### **2e**
Setiap tindakan yang dilakukan harus tercatat dalam file log bernama logmucatatsini.txt di luar file .zip yang akan diberikan kepada Nana. Hal ini bertujuan agar pekerjaan yang telah dilakukan dapat dilaporkan kepada Germa.

Format yang harus digunakan untuk logging adalah sebagai berikut:

``[STATUS]::[dd]/[MM]/[yyyy]-[HH]:[mm]:[ss]::[CMD]::[DESC]``

Dengan penjelasan sebagai berikut:

+ STATUS: SUCCESS atau FAILED.
+ dd: Tanggal dengan 2 digit.
+ MM: Bulan dengan 2 digit.
+ yyyy: Tahun dengan 4 digit.
+ HH: Jam dalam format 24 jam dengan leading 0 (misalnya 22).
+ mm: Menit dengan 2 digit.
+ ss: Detik dengan 2 digit.
+ CMD: Perintah yang digunakan (MKDIR, RENAME, RMDIR, RMFILE, dll.).
+ DESC: Keterangan tindakan, misalnya:
    + [User]-Membuat direktori x.
    + [User]-Mengubah nama dari x menjadi y.
    + [User]-Menghapus direktori x.
    + [User]-Menghapus file x.

```c
void writeLog(const char *status, const char *cmd, const char *desc)
{
  struct passwd *pws;
  int uid = getuid();
  pws = getpwuid(uid);

  time_t now;
  struct tm *timestamp;
  char datetime[200];

  // Get current timestamp
  time(&now);
  timestamp = localtime(&now);

  // Format the timestamp
  strftime(datetime, sizeof(datetime), "%d/%m/%Y-%H:%M:%S", timestamp);

  // Create log entry string
  char log_entry[2000];
  sprintf(log_entry, "%s::%s::%s::%s-%s", status, datetime, cmd, pws->pw_name, desc);

  snprintf(logFilePath, sizeof(logFilePath), "%s", "logmucatatsini.txt");
  logFile = fopen(logFilePath, "a");
  if (logFile == NULL)
  {
    perror("Failed to open log file");
  }

  fprintf(logFile, "%s\n", log_entry);

  fclose(logFile);

  // SUCCESS::17/05/2023-19:31:56::RENAME::Oky-Rename from x to y
}
```
+ Fungsi ``writeLog`` digunakan untuk menulis log ke file ``logmucatatsini.txt``.
+ Pertama, kita akan mendapatkan user yang sedang menjalankan program ini dengan menggunakan ``getuid()`` dan ``getpwuid(uid)``.
+ Setelah itu, kita akan mendapatkan waktu saat ini dengan menggunakan ``time(&now)`` dan ``localtime(&now)``.
+ Setelah itu, kita akan memformat waktu tersebut dengan menggunakan ``strftime(datetime, sizeof(datetime), "%d/%m/%Y-%H:%M:%S", timestamp)``.
+ Setelah itu, kita akan membuat string log dengan menggunakan ``sprintf(log_entry, "%s::%s::%s::%s-%s", status, datetime, cmd, pws->pw_name, desc)``.
+ Setelah itu, kita akan membuka file ``logmucatatsini.txt`` dengan menggunakan ``fopen(logFilePath, "a")``.
+ Setelah itu, kita akan menulis log ke file ``logmucatatsini.txt`` dengan menggunakan ``fprintf(logFile, "%s\n", log_entry)``.
+ Terakhir, kita akan menutup file ``logmucatatsini.txt`` dengan menggunakan ``fclose(logFile)``.

### Contoh Log
```
FAILED::31/05/2023-20:37:56::MKDIR::daffasas-Create directory /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/products/restricted_list/productMagang
SUCCESS::31/05/2023-20:41:48::RENAME::daffasas-Rename from /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/products/restricted_list to /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/products/bypass_list
SUCCESS::31/05/2023-20:42:11::MKDIR::daffasas-Create directory /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/products/bypass_list/productMagang
FAILED::31/05/2023-20:46:02::RENAME::daffasas-Rename from /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/projects/restricted_list/filePenting to /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/projects/restricted_list/restrictedFilePenting
FAILED::31/05/2023-20:46:50::RENAME::daffasas-Rename from /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/projects/restricted_list/filePenting to /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/projects/restricted_list/restrictedFilePenting
SUCCESS::31/05/2023-20:48:07::RENAME::daffasas-Rename from /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/projects/restricted_list to /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/projects/bypass_list
SUCCESS::31/05/2023-20:48:47::RENAME::daffasas-Rename from /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/projects/bypass_list/filePenting to /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/germaa/projects/bypass_list/restrictedFilePenting
SUCCESS::31/05/2023-20:50:19::RENAME::daffasas-Rename from /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/others/restrictedFileLama to /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/others/bypassFileLama
SUCCESS::31/05/2023-20:53:44::UNLINK::daffasas-Remove file /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/others/bypassFileLama/fileLama1.txt
SUCCESS::31/05/2023-20:53:44::UNLINK::daffasas-Remove file /home/daffasas/Desktop/modul4fix/nanaxgerma/src_data/others/bypassFileLama/fileLama2.txt
```

### Kendala Pengerjaan Soal 2

## 3. Secret Admirer
### Penjelasan Solusi
Penyelesaian soal ini akan berupa 2 file: `secretadmirer.c` dan `docker-compose.yml`

#### **`secreradmirer.c`**
```c
#define FUSE_USE_VERSION 30

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <ctype.h>
#include <errno.h>

#define BUFFER_SIZE 1024

int isPasswordCorrect() {
    char password[BUFFER_SIZE];
    printf("Enter password: ");
    fgets(password, BUFFER_SIZE, stdin);
    password[strcspn(password, "\n")] = '\0';  // Remove newline character

    return strcmp(password, "sisop") == 0;
}

int getFileNameLength(const char* filename) {
    int length = strlen(filename);
    int extensionLength = 0;

    // Find the position of the last dot in the filename (extension separator)
    const char* dot = strrchr(filename, '.');
    if (dot != NULL) {
        // Calculate the length of the extension
        extensionLength = length - (dot - filename) - 1;
    }

    // Subtract the extension length from the total length
    length -= extensionLength;

    return length;
}

void encodeAndRenameFiles(const char* directory) {
    DIR *dir = opendir(directory);
    if (dir == NULL) {
        printf("Failed to open directory '%s'\n", directory);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG || entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                if (entry->d_type == DT_DIR) {
                    // Generate the full path of the subdirectory
                    char subdirectory_path[BUFFER_SIZE];
                    snprintf(subdirectory_path, BUFFER_SIZE, "%s/%s", directory, entry->d_name);

                    // Open the subdirectory
                    DIR *subdir = opendir(subdirectory_path);
                    if (subdir == NULL) {
                        printf("Failed to open subdirectory '%s'\n", subdirectory_path);
                        continue;
                    }

                    // Recursively process the subdirectory
                    encodeAndRenameFiles(subdirectory_path);
    
                    // Close the subdirectory
                    closedir(subdir);
                }

                // Check if the name starts with L, U, T, or H (case-insensitive)
                char first_char = tolower(entry->d_name[0]);
                int should_encode = (first_char == 'l' || first_char == 'u' || first_char == 't' || first_char == 'h');

                // Generate the file or folder path
                char path[BUFFER_SIZE];
                snprintf(path, BUFFER_SIZE, "%s/%s", directory, entry->d_name);

                char name[BUFFER_SIZE];
                snprintf(name, BUFFER_SIZE, "%s", entry->d_name);

                // Perform other modifications (ASCII, lowercase, uppercase)
                size_t name_length = getFileNameLength(name);

                snprintf(path, BUFFER_SIZE, "%s/%s", directory, name);

                if (entry->d_type == DT_REG) {
                    // Change to lowercase
                    for (int i = 0; name[i] != '\0'; i++) {
                        name[i] = tolower(name[i]);
                    }
                } else if (entry->d_type == DT_DIR) {
                    // Change to uppercase
                    for (int i = 0; entry->d_name[i] != '\0'; i++) {
                        name[i] = toupper(name[i]);
                    }
                }

                if (name_length > 4) {
                    // Generate the new path with modified name
                    char new_path[BUFFER_SIZE];
                    snprintf(new_path, BUFFER_SIZE, "%s/%s", directory, name);

                    // Rename the file or folder
                    if (rename(path, new_path) == 0) {
                        printf("Renamed '%s' to '%s'\n", path, new_path);
                        snprintf(path, BUFFER_SIZE, "%s", new_path);
                    } else {
                        printf("Failed to rename '%s'\n", path);
                    }
                } else {
                    // Create a copy of the original filename
                    char newFilename[BUFFER_SIZE];
                    char newName[BUFFER_SIZE];
                    strcpy(newFilename, name);

                    int i;
                    // Convert each letter to binary ASCII representation
                    char binary[9];  // 8 bits + null terminator
                    for (i=0; newFilename[i] != '.'; i++) {
                        // Get the binary ASCII representation
                        int ascii = (int)newFilename[i];
                        for (int j = 7; j >= 0; j--) {
                            if (ascii % 2 == 0)
                            {
                                binary[j] = '0';
                            }
                            else
                            {
                                binary[j] = '1';
                            }
                            ascii = ascii / 2;
                        }
                        binary[8] = '\0';
                        // Replace the letter with binary ASCII representation in the new filename
                        strcat(newName, binary);
                        strcat(newName, " ");
                    }

                    char new_path[BUFFER_SIZE];
                    snprintf(path, BUFFER_SIZE, "%s/%s", directory, name);
                    snprintf(new_path, BUFFER_SIZE, "%s/%s", directory, newName);

                    // Rename the file
                    if (rename(path, new_path) == 0) {
                        printf("File renamed successfully to: %s\n", newName);
                        snprintf(path, BUFFER_SIZE, "%s", new_path);
                    } else {
                        printf("Failed to rename the file.\n");
                    }
                }

                // Encode the file or folder name
                if (should_encode) {
                        if (entry->d_type == DT_REG) {
                            if (isPasswordCorrect()) {
                                // Encode the file content into base64
                                FILE *file = fopen(path, "rb");
                                if (file != NULL) {
                                    fseek(file, 0, SEEK_END);
                                    long file_size = ftell(file);
                                    fseek(file, 0, SEEK_SET);

                                    unsigned char *file_data = (unsigned char *)malloc(file_size);
                                    if (file_data != NULL) {
                                        fread(file_data, 1, file_size, file);

                                        // Encode the file content
                                        BIO *b64_file = BIO_new(BIO_f_base64());
                                        BIO *bio_file = BIO_new(BIO_s_mem());
                                        BIO_push(b64_file, bio_file);

                                        BIO_write(b64_file, file_data, file_size);
                                        BIO_flush(b64_file);

                                        BUF_MEM *buffer_file;
                                        BIO_get_mem_ptr(b64_file, &buffer_file);

                                        // Write the encoded file content to the new file
                                        FILE *encoded_file = fopen(path, "wb");
                                        if (encoded_file != NULL) {
                                            fwrite(buffer_file->data, 1, buffer_file->length, encoded_file);
                                            fclose(encoded_file);
                                            printf("Encoded content of '%s'\n", path);
                                        } else {
                                            printf("Failed to encode file '%s'\n", path);
                                        }

                                        // Cleanup
                                        free(file_data);
                                        BIO_free_all(b64_file);
                                    } else {
                                        printf("Failed to allocate memory for file data\n");
                                    }

                                    fclose(file);
                                } else {
                                    printf("Failed to open file '%s'\n", path);
                                }
                            } else {
                                printf("Incorrect password. Skipping encoding for file '%s'\n", path);
                            }
                        }
                }
            }
        }
    }

    closedir(dir);
}

#define DIR_NAME "fuse_dir"
#define FILE_NAME "fuse_file"
#define FILE_CONTENT "Hello, World!\n"

// Implement the file operations

// mkdir operation
static int fuse_mkdir(const char *path, mode_t mode)
{
    // Create the directory with the specified mode
    int res = mkdir(DIR_NAME, mode);
    if (res == -1)
        return -errno;

    return 0;
}

// cat operation
static int fuse_cat(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    size_t len = strlen(FILE_CONTENT);
    if (offset < len) {
        if (offset + size > len)
            size = len - offset;
        memcpy(buf, FILE_CONTENT + offset, size);
    } else {
        size = 0;
    }

    return size;
}

// mv operation
static int fuse_mv(const char *from, const char *to)
{
    int res = rename(from, to);
    if (res == -1)
        return -errno;

    return 0;
}

static int ls_readdir(const char* path, void* buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info* fi)
{
    DIR* dir = opendir(path);
    if (dir == NULL) {
        return -errno;
    }

    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        if (filler(buf, entry->d_name, NULL, 0) != 0) {
            break;
        }
    }

    closedir(dir);
    return 0;
}

// Define the operations for our filesystem
static struct fuse_operations fuse_ops = {
    .mkdir = fuse_mkdir,
    .read = fuse_cat,
    .rename = fuse_mv,
    .readdir = ls_readdir
};


int main(int argc, char *argv[]) {
    encodeAndRenameFiles("inifolderetc");

    return fuse_main(argc, argv, &fuse_ops, "nonempty");
}
```
Untuk menjalankan `secretadmirer.c` ini diperlukan compile dengan cara menggunakan `gcc -o secretadmirer secretadmirer.c 'pkg-config fuse --cflags --libs' -lssl -lcrypto`.

Berikut adalah penjelasan dari bagian-bagian kode pada `secretadmirer.c`:
```c
int isPasswordCorrect() {
    char password[BUFFER_SIZE];
    printf("Enter password: ");
    fgets(password, BUFFER_SIZE, stdin);
    password[strcspn(password, "\n")] = '\0';  // Remove newline character

    return strcmp(password, "sisop") == 0;
}
```
- Penerimaan input untuk password pembukaan file  
- Apabila input sesuai, maka akan mengembalikan nilai 1 (TRUE), jika tidak maka akan mengembalikan nilai 0 (FALSE)

```c
int getFileNameLength(const char* filename) {
    int length = strlen(filename);
    int extensionLength = 0;

    // Find the position of the last dot in the filename (extension separator)
    const char* dot = strrchr(filename, '.');
    if (dot != NULL) {
        // Calculate the length of the extension
        extensionLength = length - (dot - filename) - 1;
    }

    // Subtract the extension length from the total length
    length -= extensionLength;

    return length;
}
```
- Mendapatkan panjang nama suatu file tanpa menghitung extension file tersebut  
- Panjang extension didapat dengan mengurangkan panjang awal dengan index dimana ditemukan titik kemudian dikurangkan kembali dengan 1 karena sifat string adalah 0-index  
- Panjang nama tanpa extension didapat dengan mengurangi panjang awal dengan panjang extension

```c
void encodeAndRenameFiles(const char* directory) {
    DIR *dir = opendir(directory);
    if (dir == NULL) {
        printf("Failed to open directory '%s'\n", directory);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG || entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
```
- Pertama, dibuka directory dengan fungsi opendir(), jika gagal maka pointer dir akan bernilai NULL  
- Lakukan iterasi untuk semua file di directory saat ini, hal ini dilakukan dengan baris `while ((entry = readdir(dir)) != NULL)`, sebab readdir() akan bergerak dari file pertama hingga terakhir dan setelah itu akan gagal dan mengembalikan NULL  
- Kemudian dilakukan pengecekan apakah memang file yang saat ini dicek bertipe file / folder  
- Setelah itu dilakukan pengecekan apakah nama file saat ini bukanlah "." atau ".."  

```c
if (entry->d_type == DT_DIR) {
    // Generate the full path of the subdirectory
    char subdirectory_path[BUFFER_SIZE];
    snprintf(subdirectory_path, BUFFER_SIZE, "%s/%s", directory, entry->d_name);

    // Open the subdirectory
    DIR *subdir = opendir(subdirectory_path);
    if (subdir == NULL) {
        printf("Failed to open subdirectory '%s'\n", subdirectory_path);
        continue;
    }

    // Recursively process the subdirectory
    encodeAndRenameFiles(subdirectory_path);

    // Close the subdirectory
    closedir(subdir);
}
```
- Apabila saat ini merupakan folder, maka akan dilakukan iterasi terlebih dahulu untuk folder ini (disini saya menggunakan pendekatan DFS dalam iterasi foldernya)  
- Dapatkan path ke folder saat ini kemudian simpan di variabel subdirectory_path  
- Buka directory dengan opendir() kemudian panggil fungsi `encodeAndRenameFiles(subdirectory_path)`  
- Tutup subdirectory tersebut  

```c
char path[BUFFER_SIZE];
snprintf(path, BUFFER_SIZE, "%s/%s", directory, entry->d_name);

char name[BUFFER_SIZE];
snprintf(name, BUFFER_SIZE, "%s", entry->d_name);

// Perform other modifications (ASCII, lowercase, uppercase)
size_t name_length = getFileNameLength(name);

snprintf(path, BUFFER_SIZE, "%s/%s", directory, name);

if (entry->d_type == DT_REG) {
    // Change to lowercase
    for (int i = 0; name[i] != '\0'; i++) {
        name[i] = tolower(name[i]);
    }
} else if (entry->d_type == DT_DIR) {
    // Change to uppercase
    for (int i = 0; entry->d_name[i] != '\0'; i++) {
        name[i] = toupper(name[i]);
    }
}

if (name_length > 4) {
    // Generate the new path with modified name
    char new_path[BUFFER_SIZE];
    snprintf(new_path, BUFFER_SIZE, "%s/%s", directory, name);

    // Rename the file or folder
    if (rename(path, new_path) == 0) {
        printf("Renamed '%s' to '%s'\n", path, new_path);
        snprintf(path, BUFFER_SIZE, "%s", new_path);
    } else {
        printf("Failed to rename '%s'\n", path);
    }
} else {
    // Create a copy of the original filename
    char newFilename[BUFFER_SIZE];
    char newName[BUFFER_SIZE];
    strcpy(newFilename, name);

    int i;
    // Convert each letter to binary ASCII representation
    char binary[9];  // 8 bits + null terminator
    for (i=0; newFilename[i] != '.'; i++) {
        // Get the binary ASCII representation
        int ascii = (int)newFilename[i];
        for (int j = 7; j >= 0; j--) {
            if (ascii % 2 == 0)
            {
                binary[j] = '0';
            }
            else
            {
                binary[j] = '1';
            }
            ascii = ascii / 2;
        }
        binary[8] = '\0';
        // Replace the letter with binary ASCII representation in the new filename
        strcat(newName, binary);
        strcat(newName, " ");
    }

    char new_path[BUFFER_SIZE];
    snprintf(path, BUFFER_SIZE, "%s/%s", directory, name);
    snprintf(new_path, BUFFER_SIZE, "%s/%s", directory, newName);

    // Rename the file
    if (rename(path, new_path) == 0) {
        printf("File renamed successfully to: %s\n", newName);
        snprintf(path, BUFFER_SIZE, "%s", new_path);
    } else {
        printf("Failed to rename the file.\n");
    }
}
```
- Dapatkan path ke file / folder saat ini beserta namanya  
- Cek panjang nama file / folder dengan memanggil fungsi getFileNameLength(name)  
- Apabila tipe saat ini adalah file, maka ubah semua karakter nama menjadi lowercase dengan fungsi tolower(). Apabila tipe saat ini adalah folder, maka ubah semua karakter nama menjadi uppercase dengan fungsi toupper()  
- Apabila panjang nama file tanpa extension lebih dari 4, maka dapat langsung dilakukan rename. Dapatkan path baru ke file/folder tersebut kemudian panggil fungsi rename() dan jika berhasil, update nilai variabel path  
- Apabila panjang nama file tanpa extension 4 atau kurang, maka perlu diubah terlebih dahulu ke ASCII. Untuk setiap karakter, dapatkan nilai ASCIInya kemudian ubah menjadi binary kemudian tambahkan ke variabel lain (newName) beserta spasi. Dapatkan path baru ke folder tersebut kemudian panggil fungsi rename() dan jika berhasil, update nilai variabel path  

```c
char first_char = tolower(entry->d_name[0]);
int should_encode = (first_char == 'l' || first_char == 'u' || first_char == 't' || first_char == 'h');

...

if (should_encode) {
    if (entry->d_type == DT_REG) {
        if (isPasswordCorrect()) {
            // Encode the file content into base64
            FILE *file = fopen(path, "rb");
            if (file != NULL) {
                fseek(file, 0, SEEK_END);
                long file_size = ftell(file);
                fseek(file, 0, SEEK_SET);

                unsigned char *file_data = (unsigned char *)malloc(file_size);
                if (file_data != NULL) {
                    fread(file_data, 1, file_size, file);

                    // Encode the file content
                    BIO *b64_file = BIO_new(BIO_f_base64());
                    BIO *bio_file = BIO_new(BIO_s_mem());
                    BIO_push(b64_file, bio_file);

                    BIO_write(b64_file, file_data, file_size);
                    BIO_flush(b64_file);

                    BUF_MEM *buffer_file;
                    BIO_get_mem_ptr(b64_file, &buffer_file);

                    // Write the encoded file content to the new file
                    FILE *encoded_file = fopen(path, "wb");
                    if (encoded_file != NULL) {
                        fwrite(buffer_file->data, 1, buffer_file->length, encoded_file);
                        fclose(encoded_file);
                        printf("Encoded content of '%s'\n", path);
                    } else {
                        printf("Failed to encode file '%s'\n", path);
                    }

                    // Cleanup
                    free(file_data);
                    BIO_free_all(b64_file);
                } else {
                    printf("Failed to allocate memory for file data\n");
                }

                fclose(file);
            } else {
                printf("Failed to open file '%s'\n", path);
            }
        } else {
            printf("Incorrect password. Skipping encoding for file '%s'\n", path);
        }
    }
}
```
- Sebelum dilakukan rename, dapatkan karakter pertama dari nama file/folder kemudian ubah ke lowercase karena L,U,T,H tidaklah case sensitive  
- Cek apakah karakter tersebut adalah l, u, t, atau h. Jika iya maka variabel should_encode bernilai 1 (TRUE), jika tidak, maka variabel should_encode bernilai 0 (FALSE)  
- Jika should_encode bernilai 1, cek apakah tipe saat ini adalah file. Jika iya, maka panggil fungsi isPasswordCorrect() untuk meminta & mengecek password. Jika password benar, maka lakukan enkripsi dengan base64  

```c
// mkdir operation
static int fuse_mkdir(const char *path, mode_t mode)
{
    // Create the directory with the specified mode
    int res = mkdir(DIR_NAME, mode);
    if (res == -1)
        return -errno;

    return 0;
}

// cat operation
static int fuse_cat(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    size_t len = strlen(FILE_CONTENT);
    if (offset < len) {
        if (offset + size > len)
            size = len - offset;
        memcpy(buf, FILE_CONTENT + offset, size);
    } else {
        size = 0;
    }

    return size;
}

// mv operation
static int fuse_mv(const char *from, const char *to)
{
    int res = rename(from, to);
    if (res == -1)
        return -errno;

    return 0;
}

static int ls_readdir(const char* path, void* buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info* fi)
{
    DIR* dir = opendir(path);
    if (dir == NULL) {
        return -errno;
    }

    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        if (filler(buf, entry->d_name, NULL, 0) != 0) {
            break;
        }
    }

    closedir(dir);
    return 0;
}

// Define the operations for our filesystem
static struct fuse_operations fuse_ops = {
    .mkdir = fuse_mkdir,
    .read = fuse_cat,
    .rename = fuse_mv,
    .readdir = ls_readdir
};
```
- Pengaplikasian FUSE untuk mkdir (fuse_mkdir), cat (fuse_cat), mv (fuse_mv) dan ls (ls_readdir)

#### **`docker-compose.yml`**
```c
version: '2'

services:
  dhafin:
    image: ubuntu:bionic
    volumes:
      - ./inifolderetc:/inifolderetc
      - secretadmirer:secretadmirer

  normal:
    image: ubuntu:bionic
    volumes:
      - ./inifolderetc:/inifolderetc
```
Cara mengoperasikan `docker-compose.yml` diatas adalah sebagai berikut:
- Compile docker-compose tersebut dengan command `docker-compose up`  
- Jalankan perintah berikut `docker exec dhafin secretadmirer /inifolderetc`, kemudian masukkan password ketika diminta  
- Jalankan perintah berikut `docker pull fhinnn/sisop23:latest` kemudian `docker run fhinnn/sisop23:latest` untuk menjalankan image fhinnn/sisop23

Berikut adalah penjelasan dari `docker-compose.yml` diatas:
- Terdapat 2 container yaitu dhafin dan normal  
- Kedua container menggunakan image ubuntu:bionic  
- Container dhafin berisi 1 file program (secretadmirer) dan 1 folder (inifolderetc)  
- Container normal berisi 1 folder (inifolderetc)  
### Screenshoot Solusi Soal 3
- Contoh eksekusi program 
![1](https://github.com/mavaldi/image-placeeee/blob/main/.1.png?raw=true)
- Rename file/folder

![2](https://github.com/mavaldi/image-placeeee/blob/main/.2.png?raw=true)
![3](https://github.com/mavaldi/image-placeeee/blob/main/.3.png?raw=true)
- Enkripsi base64
![4](https://github.com/mavaldi/image-placeeee/blob/main/.4.png?raw=true)
![5](https://github.com/mavaldi/image-placeeee/blob/main/.5.png?raw=true)
- Eksekusi image fhinnn/sisop23
![6](https://github.com/mavaldi/image-placeeee/blob/main/.6.png?raw=true)
### Kendala Pengerjaan Soal 3
- Terkadang terjadi error pada enkripsi dan rename file/folder

