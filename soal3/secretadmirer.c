#define FUSE_USE_VERSION 30

#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <ctype.h>
#include <errno.h>

#define BUFFER_SIZE 1024

int isPasswordCorrect() {
    char password[BUFFER_SIZE];
    printf("Enter password: ");
    fgets(password, BUFFER_SIZE, stdin);
    password[strcspn(password, "\n")] = '\0';  // Remove newline character

    return strcmp(password, "sisop") == 0;
}

int getFileNameLength(const char* filename) {
    int length = strlen(filename);
    int extensionLength = 0;

    // Find the position of the last dot in the filename (extension separator)
    const char* dot = strrchr(filename, '.');
    if (dot != NULL) {
        // Calculate the length of the extension
        extensionLength = length - (dot - filename) - 1;
    }

    // Subtract the extension length from the total length
    length -= extensionLength;

    return length;
}

void encodeAndRenameFiles(const char* directory) {
    DIR *dir = opendir(directory);
    if (dir == NULL) {
        printf("Failed to open directory '%s'\n", directory);
        return;
    }

    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG || entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") != 0 && strcmp(entry->d_name, "..") != 0) {
                if (entry->d_type == DT_DIR) {
                    // Generate the full path of the subdirectory
                    char subdirectory_path[BUFFER_SIZE];
                    snprintf(subdirectory_path, BUFFER_SIZE, "%s/%s", directory, entry->d_name);

                    // Open the subdirectory
                    DIR *subdir = opendir(subdirectory_path);
                    if (subdir == NULL) {
                        printf("Failed to open subdirectory '%s'\n", subdirectory_path);
                        continue;
                    }

                    // Recursively process the subdirectory
                    encodeAndRenameFiles(subdirectory_path);
    
                    // Close the subdirectory
                    closedir(subdir);
                }

                // Check if the name starts with L, U, T, or H (case-insensitive)
                char first_char = tolower(entry->d_name[0]);
                int should_encode = (first_char == 'l' || first_char == 'u' || first_char == 't' || first_char == 'h');

                // Generate the file or folder path
                char path[BUFFER_SIZE];
                snprintf(path, BUFFER_SIZE, "%s/%s", directory, entry->d_name);

                char name[BUFFER_SIZE];
                snprintf(name, BUFFER_SIZE, "%s", entry->d_name);

                // Perform other modifications (ASCII, lowercase, uppercase)
                size_t name_length = getFileNameLength(name);

                snprintf(path, BUFFER_SIZE, "%s/%s", directory, name);

                if (entry->d_type == DT_REG) {
                    // Change to lowercase
                    for (int i = 0; name[i] != '\0'; i++) {
                        name[i] = tolower(name[i]);
                    }
                } else if (entry->d_type == DT_DIR) {
                    // Change to uppercase
                    for (int i = 0; entry->d_name[i] != '\0'; i++) {
                        name[i] = toupper(name[i]);
                    }
                }

                if (name_length > 4) {
                    // Generate the new path with modified name
                    char new_path[BUFFER_SIZE];
                    snprintf(new_path, BUFFER_SIZE, "%s/%s", directory, name);

                    // Rename the file or folder
                    if (rename(path, new_path) == 0) {
                        printf("Renamed '%s' to '%s'\n", path, new_path);
                        snprintf(path, BUFFER_SIZE, "%s", new_path);
                    } else {
                        printf("Failed to rename '%s'\n", path);
                    }
                } else {
                    // Create a copy of the original filename
                    char newFilename[BUFFER_SIZE];
                    char newName[BUFFER_SIZE];
                    strcpy(newFilename, name);

                    int i;
                    // Convert each letter to binary ASCII representation
                    char binary[9];  // 8 bits + null terminator
                    for (i=0; newFilename[i] != '.'; i++) {
                        // Get the binary ASCII representation
                        int ascii = (int)newFilename[i];
                        for (int j = 7; j >= 0; j--) {
                            if (ascii % 2 == 0)
                            {
                                binary[j] = '0';
                            }
                            else
                            {
                                binary[j] = '1';
                            }
                            ascii = ascii / 2;
                        }
                        binary[8] = '\0';
                        // Replace the letter with binary ASCII representation in the new filename
                        strcat(newName, binary);
                        strcat(newName, " ");
                    }

                    char new_path[BUFFER_SIZE];
                    snprintf(path, BUFFER_SIZE, "%s/%s", directory, name);
                    snprintf(new_path, BUFFER_SIZE, "%s/%s", directory, newName);

                    // Rename the file
                    if (rename(path, new_path) == 0) {
                        printf("File renamed successfully to: %s\n", newName);
                        snprintf(path, BUFFER_SIZE, "%s", new_path);
                    } else {
                        printf("Failed to rename the file.\n");
                    }
                }

                // Encode the file or folder name
                if (should_encode) {
                        if (entry->d_type == DT_REG) {
                            if (isPasswordCorrect()) {
                                // Encode the file content into base64
                                FILE *file = fopen(path, "rb");
                                if (file != NULL) {
                                    fseek(file, 0, SEEK_END);
                                    long file_size = ftell(file);
                                    fseek(file, 0, SEEK_SET);

                                    unsigned char *file_data = (unsigned char *)malloc(file_size);
                                    if (file_data != NULL) {
                                        fread(file_data, 1, file_size, file);

                                        // Encode the file content
                                        BIO *b64_file = BIO_new(BIO_f_base64());
                                        BIO *bio_file = BIO_new(BIO_s_mem());
                                        BIO_push(b64_file, bio_file);

                                        BIO_write(b64_file, file_data, file_size);
                                        BIO_flush(b64_file);

                                        BUF_MEM *buffer_file;
                                        BIO_get_mem_ptr(b64_file, &buffer_file);

                                        // Write the encoded file content to the new file
                                        FILE *encoded_file = fopen(path, "wb");
                                        if (encoded_file != NULL) {
                                            fwrite(buffer_file->data, 1, buffer_file->length, encoded_file);
                                            fclose(encoded_file);
                                            printf("Encoded content of '%s'\n", path);
                                        } else {
                                            printf("Failed to encode file '%s'\n", path);
                                        }

                                        // Cleanup
                                        free(file_data);
                                        BIO_free_all(b64_file);
                                    } else {
                                        printf("Failed to allocate memory for file data\n");
                                    }

                                    fclose(file);
                                } else {
                                    printf("Failed to open file '%s'\n", path);
                                }
                            } else {
                                printf("Incorrect password. Skipping encoding for file '%s'\n", path);
                            }
                        }
                }
            }
        }
    }

    closedir(dir);
}

#define DIR_NAME "fuse_dir"
#define FILE_NAME "fuse_file"
#define FILE_CONTENT "Hello, World!\n"

// Implement the file operations

// mkdir operation
static int fuse_mkdir(const char *path, mode_t mode)
{
    // Create the directory with the specified mode
    int res = mkdir(DIR_NAME, mode);
    if (res == -1)
        return -errno;

    return 0;
}

// cat operation
static int fuse_cat(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    size_t len = strlen(FILE_CONTENT);
    if (offset < len) {
        if (offset + size > len)
            size = len - offset;
        memcpy(buf, FILE_CONTENT + offset, size);
    } else {
        size = 0;
    }

    return size;
}

// mv operation
static int fuse_mv(const char *from, const char *to)
{
    int res = rename(from, to);
    if (res == -1)
        return -errno;

    return 0;
}

static int ls_readdir(const char* path, void* buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info* fi)
{
    DIR* dir = opendir(path);
    if (dir == NULL) {
        return -errno;
    }

    struct dirent* entry;
    while ((entry = readdir(dir)) != NULL) {
        if (filler(buf, entry->d_name, NULL, 0) != 0) {
            break;
        }
    }

    closedir(dir);
    return 0;
}

// Define the operations for our filesystem
static struct fuse_operations fuse_ops = {
    .mkdir = fuse_mkdir,
    .read = fuse_cat,
    .rename = fuse_mv,
    .readdir = ls_readdir
};


int main(int argc, char *argv[]) {
    encodeAndRenameFiles("inifolderetc");

    return fuse_main(argc, argv, &fuse_ops, "nonempty");
}
