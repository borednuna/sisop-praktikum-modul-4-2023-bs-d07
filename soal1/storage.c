#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_LINE_LENGTH 1024

int main() {
  // Download kaggle dataset with system call
  system("kaggle datasets download -d bryanb/fifa-player-stats-database");

  // Unzip the downloaded file if not unzipped yet
  system("unzip -o fifa-player-stats-database.zip");

  // Read FIFA23_official_data.csv
  FILE* file = fopen("./FIFA23_official_data.csv", "r");
  if (file == NULL) {
    printf("Failed to open the CSV file.\n");
    return 1;
  }

  char line[MAX_LINE_LENGTH];
  fgets(line, sizeof(line), file); // Skip header

  while (fgets(line, sizeof(line), file) != NULL) {
    char id[MAX_LINE_LENGTH];
    char name[MAX_LINE_LENGTH];
    int age;
    char photo[MAX_LINE_LENGTH];
    char nationality[MAX_LINE_LENGTH];
    char flag[MAX_LINE_LENGTH];
    int overall;
    int potential;
    char club[MAX_LINE_LENGTH];

    // Tokenize the line
    char* token = strtok(line, ",");
    strcpy(id, token);

    token = strtok(NULL, ",");
    strcpy(name, token);

    token = strtok(NULL, ",");
    age = atoi(token);

    token = strtok(NULL, ",");
    strcpy(photo, token);

    token = strtok(NULL, ",");
    strcpy(nationality, token);

    token = strtok(NULL, ",");
    strcpy(flag, token);

    token = strtok(NULL, ",");
    overall = atoi(token);

    token = strtok(NULL, ",");
    potential = atoi(token);

    token = strtok(NULL, ",");
    strcpy(club, token);

    // Skip if age >= 25 or potential <= 85
    if (age >= 25 || potential <= 85) {
      continue;
    }

    printf("Name: %s\n", name);
    printf("Age: %d\n", age);
    printf("Photo: %s\n", photo);
    printf("Nationality: %s\n", nationality);
    printf("Overall: %d\n", overall);
    printf("Potential: %d\n", potential);
    printf("Club: %s\n", club);

    printf("\n");
  }

  fclose(file);

  return 0;
}
